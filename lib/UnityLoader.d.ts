// Unity 2017.2.0f3 // Editor/Data/PlaybackEngines/WebGLSupport/BuildTools/UnityLoader.js

interface UnityInstanceBase {
  onProgress?: (unityInstance: UnityInstance, progress: number) => void;
  compatibilityCheck?: (
    unityInstance: UnityInstance,
    onsuccess: (this: GlobalEventHandlers, ev: MouseEvent) => any,
    onerror: (this: GlobalEventHandlers, ev: MouseEvent) => any,
  ) => void;

  Module?: UnityModuleBase;

  SetFullscreen?: (fullscreen: number) => void;
  SendMessage?: (gameObject: string, func: string, param?: string | number) => void;

  popup?: (message: string, buttons: UnityButton[]) => void;

  width?: string;
  height?: string;
}
interface UnityInstance extends UnityInstanceBase {
  url: string;

  Module: UnityModule;

  container: HTMLElement;
}

interface UnityModuleBase {
  preRun?: (() => void)[];
  postRun?: (() => void)[];
  print?: (message?: any) => void;
  printErr?: (message?: any) => void;
  resolveBuildUrl?: (buildUrl: string) => string;

  preInit?: (() => void)[];
  noInitialRun?: boolean;
  monitorRunDependencies?: (runDependencies: number) => void;
  preMainLoop?: () => boolean;
  postMainLoop?: () => void;
  onFullscreen?: (isFullscreen: boolean) => void;
  forcedAspectRatio?: number;
  freePreloadedMediaOnUse?: boolean;
  onRuntimeInitialized?: () => void;
  arguments?: string[];
  onExit?: (status: number) => void;
}
interface UnityModule extends UnityModuleBase {
  gameInstance: UnityInstance; // TODO check if renamed in 2019.1

  canvas: HTMLCanvasElement;

  // from build json
  TOTAL_MEMORY: number;
  dataUrl: string;
  asmCodeUrl: string;
  wasmCodeUrl?: string;
  asmMemoryUrl: string;
  asmFrameworkUrl: string;
  asmLibraryUrl?: string;
  graphicsAPI: string[];
  webglContextAttributes: { preserveDrawingBuffer: boolean };
  splashScreenStyle: string;
  backgroundColor?: string;

  backgroundUrl?: string;
  useWasm?: boolean;
  usingWasm?: boolean;
  buildDownloadProgress: {
    [idJob: string]: UnityDownloadProgress;
    'downloadWasmCode'?: UnityDownloadProgress;
    'downloadWasmFramework'?: UnityDownloadProgress;
    'downloadAsmCode'?: UnityDownloadProgress;
    'downloadAsmMemory'?: UnityDownloadProgress;
    'downloadAsmFramework'?: UnityDownloadProgress;
    'downloadData'?: UnityDownloadProgress;
  };
  Jobs: {
    [idJob: string]: UnityJob;
    'downloadWasmCode'?: UnityJob;
    'downloadWasmFramework'?: UnityJob;
    'downloadAsmCode'?: UnityJob;
    'downloadAsmMemory'?: UnityJob;
    'downloadAsmFramework'?: UnityJob;
    'downloadData'?: UnityJob;
    'processWasmCode'?: UnityJob;
    'processWasmFramework'?: UnityJob;
    'processAsmCode'?: UnityJob;
    'processAsmMemory'?: UnityJob;
    'processAsmFramework'?: UnityJob;
    'processData'?: UnityJob;
    'setupIndexedDB'?: UnityJob;
  };
  memoryInitializerRequest: {
    addEventListener: (type: string, callback: () => void) => void;
    callback: () => void;
    status: number;
    response: any;
  };
  dynamicLibraries?: string[];
  addRunDependency: (id: string) => void;
  removeRunDependency: (id: string) => void;

  onFullScreen?: (isFullscreen: boolean) => void;
  SetFullscreen: (fullscreen: number) => void;
  SendMessage: (gameObject: string, func: string, param?: string | number) => void;

  preloadPlugins; // []
  preloadedImages;
  preloadedAudios;
  videoInstances;

  Sockets;
  DNS;
  GL;
  EXCEPTIONS;
  JSEvents;
  PATH;
  TTY;
  MEMFS;
  IDBFS;
  NODEFS;
  WORKERFS;
  FS;
  SYSCALLS;
  WEBAudio;
  webcam;
  PROCINFO;
  fs;
  Browser;
  ENV;
  SDL;
  websocket;
  SOCKFS;
  DLFCN;
  Protocols;
  ctx;
  PROXYFS;
  GLUT;
  EGL;
  AL;
  GLFW;
  GLEW;
  IDBStore;
  WebVR;
  UNETWebSocketsInstances;

  unityFileSystemInit?: () => void;
  wasmJSMethod;
  wasmTextFile;
  wasmBinaryFile;
  asmjsCodeFile;
  wasmBinary;
  wasmMemory;
  asm;
  asmPreload;
  newBuffer;
  reallocBuffer;
  wasmTableSize;
  wasmMaxTableSize;
  wasmTable;
  demangle;
  inspect;

  logReadFiles;
  wr;
  elementPointerLock;
  setImmediates;
  keyboardListeningElement;
  doNotCaptureKeyboard;
  noFSInit;
  runPostSets;
  locateFile;
  memoryInitializerPrefixURL;

  stdin?;
  stdout?;
  stderr?;

  readBinary;
  /**@deprecated Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental
   * effects to the end user's experience. For more help, check https://xhr.spec.whatwg.org/.
   */
  read: (url: string) => string;
  readAsync: (
    url: string,
    onload: (response: any) => void,
    onerror: (this: XMLHttpRequest, ev: ProgressEvent) => any,
  ) => void;
  load;
  ENVIRONMENT: 'WEB' | 'WORKER' | 'NODE' | 'SHELL';
  setWindowTitle: (title: string) => void;
  thisProgram: string;

  Runtime;
  ccall;
  cwrap;
  setValue;
  getValue;
  allocate;
  getMemory: (size: number) => number;
  Pointer_stringify: (ptr: number, length: number) => string;
  AsciiToString;
  stringToAscii;
  UTF8ArrayToString;
  UTF8ToString;
  stringToUTF8Array;
  stringToUTF8;
  lengthBytesUTF8: (str: string) => number;
  extraStackTrace;
  stackTrace;
  buffer;
  HEAP8: Int8Array;
  HEAP16: Int16Array;
  HEAP32: Int32Array;
  HEAPU8: Uint8Array;
  HEAPU16: Uint16Array;
  HEAPU32: Uint32Array;
  HEAPF32: Float32Array;
  HEAPF64: Float64Array;
  HEAP;

  addOnPreRun;
  addOnInit;
  addOnPreMain;
  addOnExit;
  addOnPostRun;

  intArrayFromString;
  intArrayToString: (array: number[]) => string;
  writeStringToMemory;
  writeArrayToMemory;
  writeAsciiToMemory;

  run: (args?: string[]) => void;
  calledRun: boolean;
  callMain: (args?: string[]) => void;
  exit: (status: number) => void;
  noExitRuntime: boolean;
  abort: (what?: any) => void;
  setStatus: (status: string) => void;
  statusMessage;

  requestFullscreen;
  requestAnimationFrame;
  setCanvasSize;
  pauseMainLoop;
  resumeMainLoop;
  getUserMedia;
  createContext;
}


interface UnityButton {
  text: string;
  callback: (this: GlobalEventHandlers, ev: MouseEvent) => any;
}

interface UnityJob {
  callback: (Module: UnityModule, job: UnityJob) => void;
  starttime: number;
  endtime: number;
  parameters: any;
  complete: (result: any) => void;
  executed: boolean;
  result: { value: any; };
  dependencies: {
    [idJob: string]: boolean;
  };
  dependants: {
    [idJob: string]: boolean;
  };
}

interface UnityDownloadProgress {
  started: boolean;
  finished: boolean;
  lengthComputable: boolean;
  total: number;
  loaded: number;
}
