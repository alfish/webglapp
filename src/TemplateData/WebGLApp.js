// Copyright (c) 2020 Alfish. All rights reserved. https://spdx.org/licenses/BSD-3-Clause

class WebGLApp {
  /**
   * Create a new WebGLApp in the specified container.
   * It must have its `data-webglapp-*` attributes set correctly.
   * @param {HTMLElement} container The `div.webglapp-container` element.
   */
  constructor(container) {
    this.status = 'new';
    const d = container.dataset;
    this.name = d.webglappName;
    const [, width, height] = /**@type {string[]}*/(/**@type {string}*/(d.webglappLayout).match(/^(\d+)x(\d+)$/m));
    this.width = +width;
    this.height = +height;
    this.container = container;
    this.playKeys = ['Enter', ' '];

    const uc = /**@type {HTMLElement}*/(this.container.querySelector('.webglapp-unitycontainer'));
    setTimeout(() => {
      const p = /**@type {UnityInstanceBase}*/({
        onProgress: /**@type {*}*/(WebGLApp.displayLoadProgress),
        Module: {
          noInitialRun: true,
          postRun: [
            () => {
              this.status = 'ready';
              this.playIfReady();
            },
          ],
        },
      });
      const u = /**@type {UnityInstanceOfWebGLApp}*/(UnityLoader.instantiate(uc, /**@type {*}*/(d.webglappUrl), p));
      u.webglapp = this;
      this.unityInstance = u;
      this.status = 'downloading';
    });

    this.overlay = /**@type {HTMLElement}*/(this.container.querySelector('.webglapp-overlay'));

    const btnPlay = /**@type {SVGSVGElement}*/(this.overlay.querySelector('.webglapp-play-btn'));
    const togglePlay = () => {
      d.webglappStartplay = /**@type {*}*/(+!+/**@type {string}*/(d.webglappStartplay));
      this.playIfReady();
    };
    btnPlay.addEventListener('click', togglePlay, { passive: true });
    this.handleKeyPlay = /**@param {KeyboardEvent} event*/ event => {
      if (!this.playKeys.includes(event.key)) return;
      togglePlay();
      event.preventDefault();
    };
    document.addEventListener('keyup', this.handleKeyPlay);

    const fullscreenBtn = /**@type {SVGSVGElement}*/(this.overlay.querySelector('.webglapp-fullscreen-btn'));
    if (!document.fullscreenEnabled)
      fullscreenBtn.style.display = 'none';
    else
      fullscreenBtn.addEventListener('click', () => {
        if (document.fullscreenElement != null)
          document.exitFullscreen();
        else
          this.container.requestFullscreen();
      }, { passive: true });

    const progressPath = /**@type {SVGPathElement}*/(this.overlay.querySelector('path.webglapp-play-progress'));
    const progressCircle = /**@type {SVGCircleElement}*/(this.overlay.querySelector('circle.webglapp-play-progress'));
    const c = 128, r = 80;
    /**
     * Set the progress indicator.
     * @param {number} progress The fraction of progress.
     * @param {number} angle The start angle of the arc.
     */
    this.setProgressIndicator = (progress, angle = 0) => {
      if (progress < 1) {
        progressPath.setAttribute('d', svgArcPath(c, c, r, 0.25 - angle, progress, 1));
        progressCircle.setAttribute('r', /**@type {*}*/(0));
      } else {
        progressPath.setAttribute('d', '');
        progressCircle.setAttribute('r', /**@type {*}*/(r));
      }

      /**
       * A command to draw an arc of a circle using absolute angles as parameters.
       * This generates an string for the d attribute of an SVG path element.
       * @param {number} cx The x coordinate of the center of the arc.
       * @param {number} cy The y coordinate of the center of the arc.
       * @param {number} r The radius of the arc.
       * @param {string | number} a0 The start angle of the arc, in units defined by `revSize`. 0 is on the right.
       * If this number is a string, the start angle is not explicitly included.
       * Use a string when this segment is a continuation of a previous path.
       * @param {number} aDelta The length of the arc, in angle units relative to `revSize`.
       * An euclidean modulo operation ensures that the non-negative size of the segment is
       * within the range of `revSize`.
       * @param {number} revSize The size of a revolution (one turn).
       * If it's negative, the angles increase in clockwise direction.
       * If zero or unset, it's assumed to be 2 * PI so that you have angles in radians, counterclockwise.
       */
      function svgArcPath(cx, cy, r, a0, aDelta, revSize) {
        const _2PI = 2 * Math.PI;
        if (!revSize) revSize = _2PI;
        const a1 = +a0 + aDelta;
        const inRadians = _2PI / revSize;
        const start = a0 === +a0 ? 'M' + polarToCartesian(cx, cy, r, a0 * inRadians) : '';
        const end = polarToCartesian(cx, cy, r, a1 * inRadians);
        const arcSizeTurns = mod(aDelta / Math.abs(revSize), 1);
        const largeArcFlag = +(arcSizeTurns >= 0.5);
        const clockwiseFlag = +(revSize < 0);
        const rd = toFixed(r);
        return `${start}A${rd},${rd},0,${largeArcFlag},${clockwiseFlag},${end}`;

        /**
         * @param {number} n
         */
        function toFixed(n) { return (+n).toFixed(12).replace(/\.0*$|(\.\d*[1-9])0+$/, '$1'); }
        /**
         * @param {number} a
         * @param {number} n
         */
        function mod(a, n) { return (a % n + n) % n; }
        /**
         * @param {number} cx
         * @param {number} cy
         * @param {number} r
         * @param {number} rad
         */
        function polarToCartesian(cx, cy, r, rad) {
          return toFixed(cx + r * Math.cos(rad)) + ',' + toFixed(cy - r * Math.sin(rad));
        }
      }
    };

    WebGLApp.apps.push(this);
  }

  /**
   * Play the WebGLApp if it's ready to play, do nothing otherwise.
   */
  playIfReady() {
    if (this.status != 'ready' || !+/**@type {string}*/(this.container.dataset.webglappStartplay))
      return;
    this.overlay.hidden = true;
    this.unityInstance.Module.callMain();
    this.status = 'playing';
    document.removeEventListener('keyup', this.handleKeyPlay);
  }

  /**
   * The function used to display download progress of a WebGLApp.
   * @param {UnityInstanceOfWebGLApp} unityInstance The unityInstance property of a WebGLApp.
   * @param {number} progress The download progress.
   */
  static displayLoadProgress(unityInstance, progress) {
    const app = unityInstance.webglapp;
    app.container.dataset.webglappSplash = unityInstance.Module.splashScreenStyle;
    const d = 0.9375;
    const bdp = unityInstance.Module.buildDownloadProgress;
    for (const idJob in bdp) {
      if (!bdp[idJob].finished) {
        // display downloading
        app.setProgressIndicator(d * progress, 0);
        return;
      }
    }
    if (progress > 0) {
      // display processing
      app.status = 'processing';
      const fps = 60, angularSpeed = 1;
      const processingAnimator = {
        angle: 0,
        handle: setInterval(() => {
          if (app.status == 'processing') {
            app.setProgressIndicator(d, processingAnimator.angle);
            processingAnimator.angle += angularSpeed / fps;
          } else {
            app.setProgressIndicator(1);
            clearInterval(processingAnimator.handle);
          }
        }, 1000 / fps),
      };
    }
  }

  /**
   * Return the WebGLApp having the specified element.
   * @param {?Element} element The element inside the WebGLApp container.
   */
  static containingElement(element) {
    while (element != null) {
      const app = WebGLApp.apps.find(app => app.container == element);
      if (app)
        return app;
      element = element.parentElement;
    }
    return null;
  }

}
WebGLApp.apps = /**@type {WebGLApp[]}*/([]);

/** @typedef {UnityInstance & { webglapp: WebGLApp }} UnityInstanceOfWebGLApp */
