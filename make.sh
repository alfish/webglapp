nbin=./node_modules/.bin
mkdir -p ./dist/WebGLTemplates/WebGLApp/TemplateData/
cp -vrut ./dist/WebGLTemplates/WebGLApp/ ./src/*.*
cp -vrut ./dist/WebGLTemplates/WebGLApp/TemplateData/ ./WebGLApp.ico ./src/TemplateData/*.css
$nbin/uglifyjs ./src/TemplateData/WebGLApp.js -mo ./dist/WebGLTemplates/WebGLApp/TemplateData/WebGLApp.min.js &&
echo "WebGLApp.js -> uglifyjs -> dist"
