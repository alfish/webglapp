# WebGLApp
A WebGL template for Unity that expands the canvas on the whole browser screen.

The loading bar is around the play button.  
Click play or press SPACE or ENTER to play (after loading) or to toggle autoplay (while loading).

## Install
With npm depencies installed (`npm i`), run this in the root of this project:
```sh
./make.sh # Minify js and copy files to dist folder
./dist.sh "path/to/Unity Project" # Copy dist files to project's "Assets" folder
```

## Parameters
### AUTOPLAY
Set to 1 to automatically start playing as soon as data is loaded.  
Otherwise, interaction will be needed to start playing.  
Note that browsers might not play the audio until some user interaction occurs anyways.

## Compatibility
Doesn't work before Unity 5.6.

## License
[BSD-3-Clause]( https://spdx.org/licenses/BSD-3-Clause )  
© 2020 Alfish. All rights reserved.
